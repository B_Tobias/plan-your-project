PYP.ticket = function () {
    var myticket;
    return {
        allTicket: [],
        ticketNum: -1,
        //create new ticket(JS)
        newTicketJs: function () {
            PYP.ticket.ticketNum++;
            var ticketID = PYP.ticket.ticketNum;
            var TicketClass = function (myPosX, myPosY) {
                return {
                    id: ticketID,
                    num: PYP.ticket.ticketNum,
                    posX: myPosX,
                    posY: myPosY,
                    dom: ""
                }
            }

            this.allTicket[this.ticketNum] = TicketClass(0, 0);
        },
        //create new ticket(DOM)
        newTicketDom: function (ticket) {
            $("body").append(`
				<div id=` + ticket.id + ` class="ticket">
					<section class="ticket-addmenu">
						<ul>
							<li>
								<div class="addImage ticket-addmenu-nohover myicon">
									<span title="Image" class="flaticon-frame-landscape"></span>
								</div>
								<input type="file" class="image-input"></input>
							</li>
							<li>
								<div class="ticket-addmenu-nohover remove-ticket myicon">
									<span title="Remove ticket" class="flaticon-rubbish-bin"></span>
								</div>
							</li>
						</ul>
					</section>
					<section class="ticket-content">
						<pre contenteditable="true">
						<div class='title-text'>My ticket :)</div>
						<div>content here</div>
						
						</pre>
					</section>
					<div class="myicon resize-ticket ticket-addmenu-nohover">
						<span title="resize" class="flaticon-vertical-resizing-option"></span>
					</div>
					<div class="myicon move-ticket ticket-addmenu-nohover">
						<span title="move" class="flaticon-move-arrows"></span>
					</div>
					
					<div class="myicon line-left-ticket line-icon ticket-addmenu-nohover">
						<span title="Add line" class="flaticon-music"></span>
					</div>
					<div class="myicon line-right-ticket line-icon ticket-addmenu-nohover">
						<span title="Add line" class="flaticon-music"></span>
					</div>
					<div class="myicon line-top-ticket line-icon ticket-addmenu-nohover">
						<span title="Add line" class="flaticon-music"></span>
					</div>
					<div class="myicon line-bottom-ticket line-icon ticket-addmenu-nohover">
						<span title="Add line" class="flaticon-music"></span>
					</div>
				</div>`
            );
            $("#" + ticket.id).css({
                'left': ticket.posX,
                'top': ticket.posY
            });

            //set id for conect points
            $("#" + ticket.id).find('.line-left-ticket').attr('id', ticket.id + 'LeftLine');
            $("#" + ticket.id).find('.line-right-ticket').attr('id', ticket.id + 'RightLine');
            $("#" + ticket.id).find('.line-top-ticket').attr('id', ticket.id + 'TopLine');
            $("#" + ticket.id).find('.line-bottom-ticket').attr('id', ticket.id + 'BottomLine');

            if (ticket.dom != 0)
                $("#" + ticket.id).html(ticket.dom);

            this.dragNDropHandler(ticket);

            PYP.ticket.saveTicketContentHandler();
            PYP.ticket.resizeHandler(ticket);
            PYP.ticket.iconPaleHandler(ticket);
            PYP.ticket.addImgHandler(ticket);
			PYP.slide.handler(ticket);
        },
        removeTicket: function (myTicket) {	//myTicket--->Jquery object

            //JS
            delete PYP.ticket.allTicket[myticket.attr('id').match(/\d+$/)];
            //DOM
            myTicket.remove();

            //remove lines
            PYP.arrow.refresh();
        },
        //remove ticket
        messageHandler: function () {
            $("#ticketClose, #confirmClose").click(function () {
                $("#removeTicketConfirm").hide();
            });
            $("#confirmRemove").click(function () {
                $("#removeTicketConfirm").hide();
                //remove ticket object
                PYP.ticket.removeTicket(myticket);
                //save dom
                PYP.storage.save();
            });
        },
        removeHandler: function () {
            $("body").on('click', '.remove-ticket', function () {
                myticket = $(this).parents('.ticket');
                $("#removeTicketConfirm").show();
            });
        },
        //ticket icon pale
        iconPaleHandler: function (ticket) {
            var $ticket = $('#' + ticket.id);
            $ticket.on('mouseover', function () {
                $(this).find(".myicon").not('.line-icon').css({
                    "color": "rgb(0,0,0)",
                    "border-color": "rgb(0,0,0)"
                });
            });
            $ticket.on('mouseleave', function () {
                $(this).find(".myicon").not('.line-icon').css({
                    "color": "rgba(0,0,0,0.1)",
                    "border-color": "rgba(0,0,0,0.1)"
                });
            });

            $ticket.find('.line-icon').on('mouseover', function () {
                $(this).css({
                    "color": "rgb(0,0,0)",
                    "border-color": "rgb(0,0,0)"
                });
            });
            $ticket.find('.line-icon').on('mouseleave', function () {
                $(this).css({
                    "color": "rgba(0,0,0,0.1)",
                    "border-color": "rgba(0,0,0,0.1)"
                });
            });
        },
        //Drag&Drop
        dragNDropHandler: function (ticket) {	//call for new tickets in DOM
            var ticketIdString = ticket.id,
                isOK = true,
                $ticketId = $("#" + ticket.id),
                ticketDom = document.getElementById(ticket.id);

            $ticketId.on('mousedown', function () {
                if ($ticketId.find('pre').is(':hover')) {	//if user click on ticket content{	//or resize-icon
                    if ($ticketId.find('img').length !== 0 && $ticketId.find('img').is(':hover')) {
                        isOK = true;
                    } else {
                        isOK = false;
                    }
                }
                if ($ticketId.find('.resize-ticket').is(':hover')) {
                    isOK = false;
                }
            });
            $ticketId.on('mousedown', function (e) {

                if (isOK) {
                    this.prevX = e.clientX;
                    this.prevY = e.clientY;
                    this.mouseDown = true;
                    ticketDom = this;
                    $(".ticket").css({
                        'z-index': '-20'
                    });
                    $(".ticket pre").css({
                        'z-index': '-19'
                    });
                    $(this).css({
                        'z-index': '10'
                    });
                    $(this).find('pre').css({
                        'z-index': '11'
                    });
                }
            });

            $(document).on('mousemove', function (e) {
                if (ticketDom.mouseDown) {
                    ticketDom.style.left = (Number(ticketDom.style.left.substring(0, ticketDom.style.left.length - 2)) + (e.clientX - ticketDom.prevX)) + "px";
                    ticketDom.style.top = (Number(ticketDom.style.top.substring(0, ticketDom.style.top.length - 2)) + (e.clientY - ticketDom.prevY)) + "px";
                }
                ticketDom.prevX = e.clientX;
                ticketDom.prevY = e.clientY;
            });

            $ticketId.on('mouseup', function (e) {
                isOK = true;

                this.mouseDown = false;
                PYP.ticket.allTicket[ticket.num]['posX'] = this.style.left;
                PYP.ticket.allTicket[ticket.num]['posY'] = this.style.top;
            });
        },
        addImgHandler: function (ticketObj) {
            var $ticket = $('#' + ticketObj.id),
                $inp,
                placeOfImg,
                $myTicket,
                ext,
                url;

            $ticket.find('.ticket-content>*').keydown(function (e) {
                //add more empty list items
                if (e.keyCode == 13 && $(this).find('br')) {
                    $(this).find('br').html('<br>&nbsp;');
                }
                if (e.keyCode == 8 && !$(this).is('pre')) {
                    $(this).find('div').unwrap();
                }
            });
            //image
            $ticket.find(".addImage").click(function () {
                console.log(this);
                $inp = $(this).next();
                $inp.click();
            });
            $ticket.find("input[type=file]").change(function () {
                $inp = $(this);
                placeOfImg = $ticket.find('pre');
                console.log(placeOfImg);
                $myTicket = $(this).parents('.ticket');

                function loadToImg(input, img) {
                    url = input.value;
                    ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                    if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            img.attr('src', e.target.result);
                            input.value = "";
                        }
                        reader.readAsDataURL(input.files[0]);
                        return true;
                    } else {
                        alert("There is a problem!(supported file types:git,png,jpg)");
                        return false;
                    }
                }

                //upload file to input element
                //create img inside ticket
                placeOfImg.append('<img draggable="false" class="empty-img content-img"></img>');
                //load image from input
                if (loadToImg($inp.get(0), $myTicket.find('.empty-img'))) {
                    $('.empty-img').removeClass('empty-img');
                }

                //save
                PYP.storage.save();
            });
        },
        saveTicketContentHandler: function () {
            $(".ticket pre").on('keypress click', function () {
                PYP.ticket.allTicket[$(this).parents('.ticket').attr('id')].dom = $(this).parents('.ticket').html();
            });
        },
        resizeHandler: function (ticket) {
            var resizer = $("#" + ticket.id).find('.resize-ticket').get(0),
                myticket = $('#' + ticket.id)[0],
                startX, startY, startWidth, startHeight;

            function initDrag(e) {
                startX = e.clientX;
                startY = e.clientY;
                startWidth = parseInt(document.defaultView.getComputedStyle(myticket).width, 10);
                startHeight = parseInt(document.defaultView.getComputedStyle(myticket).height, 10);
                document.documentElement.addEventListener('mousemove', doDrag, false);
                document.documentElement.addEventListener('mouseup', stopDrag, false);
            }

            function doDrag(e) {
                myticket.style.width = (startWidth + e.clientX - startX) + 'px';
                myticket.style.height = (startHeight + e.clientY - startY) + 'px';
            }

            function stopDrag(e) {
                document.documentElement.removeEventListener('mousemove', doDrag, false);
                document.documentElement.removeEventListener('mouseup', stopDrag, false);
            }

            resizer.addEventListener('mousedown', initDrag, false);
        },
        init: function () {
            PYP.ticket.removeHandler();
            PYP.ticket.messageHandler();

        }
    };
}();
