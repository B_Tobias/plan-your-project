﻿PYP.arrow = function () {
    function getOffset(el) {
        var _x = 0,
        _y = 0,
        _w = el.offsetWidth | 0,
        _h = el.offsetHeight | 0;
        while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
            _x += el.offsetLeft - el.scrollLeft;
            _y += el.offsetTop - el.scrollTop;
            el = el.offsetParent;
        }
        return {top: _y, left: _x, width: _w, height: _h};
    }

    function connect(div1, div2, color, thickness, id) {
        var off1 = getOffset(div1),
         off2 = getOffset(div2),
        // bottom right
         x1 = off1.left + off1.width,
         y1 = off1.top + off1.height,
        // top right
         x2 = off2.left + off2.width,
         y2 = off2.top,
        // distance
        length = Math.sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1))),
        // center
         cx = ((x1 + x2) / 2) - (length / 2),
         cy = ((y1 + y2) / 2) - (thickness / 2),
        // angle
        angle = Math.atan2((y1 - y2), (x1 - x2)) * (180 / Math.PI),
        htmlLine = "<div class='arrow' id=" + id + " style='padding:0px; margin:0px; height:" + thickness + "px; background-color:" + color + "; line-height:1px; position:absolute; left:" + cx + "px; top:" + cy + "px; width:" + length + "px; -moz-transform:rotate(" + angle + "deg); -webkit-transform:rotate(" + angle + "deg); -o-transform:rotate(" + angle + "deg); -ms-transform:rotate(" + angle + "deg); transform:rotate(" + angle + "deg);' />";
        $('body').append($(htmlLine));
    }

    return {
        lineNum: 0,
        lines: {},
        addLine: function (elStart, elEnd) {
            var myLine,
                lineSvg;
            //new arrow object
            PYP.arrow.lineNum++;
            PYP.arrow.lines['ln' + PYP.arrow.lineNum] = {
                id: 'ln' + PYP.arrow.lineNum,
                start: elStart,
                end: elEnd
            };
            myLine = PYP.arrow.lines['ln' + PYP.arrow.lineNum];
            //draw
            connect(myLine.start, myLine.end, 'black', 2, myLine.id);
        },
        removeLine: function (lineEl) {
            delete PYP.arrow.lines[lineEl.id];
            document.getElementById(lineEl.id).parentNode.removeChild(document.getElementById(lineEl.id));
        },
        refresh: function () {
            var line,
                lineObj,
				myline;
            //change line pos
            $('.arrow').remove();	//clear
            for (myLine in PYP.arrow.lines) {
                //get line
                line = document.getElementById(myLine);
                lineObj = PYP.arrow.lines[myLine];
                if (lineObj && document.body.contains(lineObj.start) && document.body.contains(lineObj.end)) {
                    connect(lineObj.start, lineObj.end, 'black', 2, lineObj.id);	//redraw
                }
            }
        },
        arrowHandler: function () {
            var starter,
                ender,
                startedLine = false;
            //set connect points
            $(document).on('click', '.line-icon', function () {
                if (!startedLine) {//false--->there is not any started line
                    //start line position
                    $(this).css('color', 'red');	//set style
                    starter = this;	//save starter node
                    startedLine = true;
                    starter.title = "click to break line drawing";
                } else {//true--->line started
                    //finish line

                    startedLine = false;

                    ender = this;
                    if (starter != ender) {
                        PYP.arrow.addLine(starter, ender);
                        startedLine = false;
                        starter.title = 'Double click to remove line';
                        ender.title = 'Double click to remove line';
                    } else {
                        startedLine = false;
                    }
                }
            });
            //set refresh on dnd
            $(document).on('mousemove', '.ticket', function () {
                if (this.mouseDown) {
                    PYP.arrow.refresh();
                }
            });

            //set remove on dbclick
            $(document).on('dblclick', '.line-icon', function () {

                var lines = $.map(PYP.arrow.lines, function (value, index) {
                        return [value];
                    }),
                    myObj = this,
					i;
                for (i = 0; i < lines.length; i++) {
                    if (myObj == lines[i].start || myObj == lines[i].end) {
                        PYP.arrow.removeLine(PYP.arrow.lines[lines[i].id]);
                        PYP.arrow.lines[lines[i].id].start.title = 'Add line';
                        PYP.arrow.lines[lines[i].id].end.title = 'Add line';
                    }
                }
            });
        }
    }
}();