PYP.menu = function () {
    return {
        //global var to save the position of menu
        lastPos: 'right-bottom',
        lastPosDom: {},
        //set menu position
        setPosDiscHandler: function () {
            function changePos(el, newPos) {//jquery object, 'left-top'|'right-bottom'|...
                var saveCssBeforeRemoveVert,
                    saveCssBeforeRemoveHoriz,
                    //new position to var
                    newPosSplit = newPos.split('-'),
                    newPosVert = newPosSplit[0],
                    newPosHoriz = newPosSplit[1],
                    //last position to var
                    lastPosSplit = PYP.menu.lastPos.split('-'),
                    lastPosVert = lastPosSplit[0],
                    lastPosHoriz = lastPosSplit[1],
                    //get old positions
                    saveCssBeforeRemoveVert = el.css(lastPosVert),
                    saveCssBeforeRemoveHoriz = el.css(lastPosHoriz),
                    mychoice,
                    menuNodes,
                    k,
                    vertPos = ['Left', 'Right'],
                    horPos = ['Top', 'Bottom'],
                    i,
                    j;

                el.attr('style', '');
                el.css(newPosVert, saveCssBeforeRemoveVert);
                el.css(newPosHoriz, saveCssBeforeRemoveHoriz);
            }

            $("#setMenuPos>div").click(function () {
                mychoice = $(this);

                //nodes inside #menu
                menuNodes = [
                    $("#menu"),
                    $("#logo"),
                    $("#setMenuPos"),
                    $("#add"),
                    $("#edit"),
                    $("#textEditMenu"),
                    $("#save"),
                    $("#open"),
                    $("#presentationMode")
                ];

                //Position
                vertPos = ['Left', 'Right'];
                horPos = ['Top', 'Bottom'];
                for (i = 0; i < 2; i++) {	//vertical
                    for (j = 0; j < 2; j++) {	//horizontal
                        if (mychoice.attr('id') == 'menuPos' + vertPos[i] + horPos[j]) {
                            //set element positions
                            for (k = 0; k < menuNodes.length; k++) {	//set changes
                                changePos(menuNodes[k], vertPos[i].toLowerCase() + '-' + horPos[j].toLowerCase());
                            }
                            //set disc border
                            mychoice.css({
                                'border-width': '3px'
                            });
                            $("#setMenuPos>div").not(mychoice).attr('style', '').css({
                                'border-width': '1px'
                            });
                            //set lastPos
                            PYP.menu.lastPos = vertPos[i].toLowerCase() + '-' + horPos[j].toLowerCase();
                            PYP.menu.lastPosDom = mychoice;
                        }
                    }
                }
            });
        },
        //show text edit menu
        textEditHandler: function () {
            $("#edit").hover(function () {
                $('#textEditMenu').css({
                    'right': '65%',
                    'bottom': '24%'
                });
                $(PYP.menu.lastPosDom).click();
                $('#textEditMenu').show();
            });
            $('#textEditMenu').mouseleave(function () {
                $('#textEditMenu').hide();
            });
            $(document).on('mouseup', 'pre[contenteditable="true"]', function (e) {
                $('#textEditMenu').css({
                    'right': e.clientY + 'px',
                    'bottom': e.clientX + 'px'
                });
                $(PYP.menu.lastPosDom).click();
                $('#textEditMenu').show();
            });
            $("#dottedList").click(function () {
                document.execCommand('insertUnorderedList');
            });
            $("#numberedList").click(function () {
                document.execCommand('insertOrderedList');
            });
            //text-edit menu
            //bold
            $("#textBold").click(function () {
                document.execCommand('bold');
            });
            //italic
            $("#textItalic").click(function () {
                document.execCommand('italic');
            });
            //underline
            $("#textUnderline").click(function () {
                document.execCommand('underline');
            });
            //color
            $('#red').click(function () {
                document.execCommand('forecolor', false, 'red');
            });
            $('#black').click(function () {
                document.execCommand('forecolor', false, 'black');
            });
            $('#green').click(function () {
                document.execCommand('forecolor', false, 'green');
            });
            $('#blue').click(function () {
                document.execCommand('forecolor', false, 'blue');
            });

            $("#textColor").mouseover(function () {
                $('.color-buble').show();
            });
            $("#textEditMenu").mouseleave(function () {
                $('.color-buble').hide();
            });
        },
        //menu functionalitys
        addTicketHandler: function () {
            $("#add").click(function () {
                PYP.ticket.newTicketJs();
                PYP.ticket.newTicketDom(PYP.ticket.allTicket[PYP.ticket.ticketNum]);
            });
        },
        init: function () {
            this.setPosDiscHandler();
            this.textEditHandler();
            this.addTicketHandler();
        }
    }
}();