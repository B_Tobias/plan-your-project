PYP.save = function () {
    return {
        save: function () {
            $("#save").click(function () {
                var filename = $("#projectName").text(),
                    text,
                    lines = jQuery.extend(true, {}, PYP.arrow.lines),
                    element = document.createElement('a');

                //objects to saved string:
                text = JSON.stringify(PYP.ticket.allTicket);
                for (var key in lines) {
                    if (typeof lines[key].start != 'string') {
                        lines[key].start = lines[key].start.id;
                        lines[key].end = lines[key].end.id;
                    }
                }
                text += 'ŸŸŸŸŸ' + JSON.stringify(lines);
                element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
                element.setAttribute('download', filename);
                element.style.display = 'none';
                document.body.appendChild(element);
                element.click();
                document.body.removeChild(element);
            });
        },
        get: function () {
            $("#open").click(function () {
                $("#openInput").click();
            });
            $("#openInput").change(function () {
                var reader = new FileReader(),
                    file = this.files[0];

                reader.onload = function (progressEvent) {
                    var resultArr = this.result.split('ŸŸŸŸŸ'),
                        tickets = resultArr[0],
                        linesData = resultArr[1];
                    //uploaded data load to storage
                    $("#projectName").text(file.name.replace('.txt', ''));	//set project name
                    localStorage.setItem("projectName", $("#projectName").text());
                    //clear JS
                    PYP.arrow.lines = {};
                    PYP.arrow.lineNum = 0;
                    PYP.ticket.allTicket = [];
                    PYP.ticket.ticketNum = -1;
                    //clear DOM
                    $('.ticket').remove();
                    $('.arrow').remove();
                    //load storage
                    PYP.storage.get({
                        'tickets': tickets,
                        'lines': linesData
                    });
                    //JS
                };
                reader.readAsText(file);

            });
        },
        handler: function () {
            PYP.save.save();
            PYP.save.get();
        },
        init: function () {
            this.handler();
        }
    };
}();