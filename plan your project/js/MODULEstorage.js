PYP.storage = function () {
    return {
        save: function () {
            var lines = jQuery.extend(true, {}, PYP.arrow.lines);		//CLONE of lines obj.
            //save tickets
            localStorage.setItem("tickets", JSON.stringify(PYP.ticket.allTicket));
            //save title
            localStorage.setItem("projectName", $("#projectName").text());

            //save lines
            for (var key in lines) {
                if (typeof lines[key].start != 'string') {
                    lines[key].start = lines[key].start.id;
                    lines[key].end = lines[key].end.id;
                }

            }
            localStorage.setItem("lines", JSON.stringify(lines));
        },
        get: function (dataObj) {
            var ticketDataJSON,
                linesDataJSON,
                ticketData,
                ticketDataKeys,
                lines;

            if (dataObj.hasOwnProperty('lines')) {
                ticketDataJSON = dataObj.tickets;
                linesDataJSON = dataObj.lines;
            } else {
                ticketDataJSON = localStorage.getItem('tickets');
                linesDataJSON = localStorage.getItem('lines');
            }

            if (linesDataJSON !== "") {
                linesData = JSON.parse(linesDataJSON);
            }
            if (ticketDataJSON !== "") {
                ticketData = JSON.parse(ticketDataJSON);
                ticketDataKeys = Object.keys(ticketData);
            }

            //get project name
            $("#projectName").text(localStorage.getItem("projectName"));

            //get tickets
            if (ticketDataJSON) {
                //JS

                PYP.ticket.allTicket = ticketData;
                PYP.ticket.ticketNum = ticketDataKeys.length;
                //DOM
                for (var key in ticketDataKeys) {
                    if (PYP.ticket.allTicket[key] != null) {
                        PYP.ticket.newTicketDom(PYP.ticket.allTicket[key]);
                    }
                }
            }
            //get lines
            if (linesDataJSON) {
                for (var key in linesData) {
                    PYP.arrow.addLine(document.getElementById(linesData[key].start), document.getElementById(linesData[key].end));
                }
            }
        },
        clear: function () {
            localStorage.setItem("tickets", "");
        },
        handler: function () {
            $('html').on('keypress mousedown', function () {
                PYP.storage.save();
            });
            $(document).ready(function () {
                PYP.storage.get({});
            });
        },
        init: function () {
            this.handler();
        }
    }
}();